window.daData = {};
window.daData.render = [];
window.daData.init = [];
window.daData.bind_actions = [];
window.daData.settings = [];
window.daData.onSave = [];
window.daData.destroy = [];
window.daData.contacts = [];
window.daData.leads = [];
window.daData.tasks = [];
window.daData.execute = function(event, widget){
	var result = true;
	for (var i = 0; i < window.daData[event].length; i++)
	{
		if (result){
			result = result && window.daData[event][i](widget);
		}
	}
	return result;
}

define(['jquery', 'lib/components/base/modal', 'https://itproblem.net/dev/students/annamuratov1987/dadata/dadata.js', './libs/jquery.collapse.js', './libs/chosen.jquery.min.js'], function($, Modal){
    var CustomWidget = function () {
    	var self = this;
    	window.Modal = Modal;
		this.callbacks = {
			render: function(){
				return window.daData.execute('render', self);
			},
			init: function(){
				return window.daData.execute('init', self);
			},
			bind_actions: function(){
				return window.daData.execute('bind_actions', self);
			},
			settings: function(){
				return window.daData.execute('settings', self);
			},
			onSave: function(){
				return window.daData.execute('onSave', self);
			},
			destroy: function(){
				return window.daData.execute('destroy', self);
			},
			contacts: {
					//select contacts in list and clicked on widget name
					selected: function(){
						return window.daData.execute('contacts', self);
					}
				},
			leads: {
					//select leads in list and clicked on widget name
					selected: function(){
						return window.daData.execute('leads', self);
					}
				},
			tasks: {
					//select taks in list and clicked on widget name
					selected: function(){
						return window.daData.execute('tasks', self);
					}
				}
		};
		return this;
    };

return CustomWidget;
});
