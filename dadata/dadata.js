var self = this;

window.daData.render.push(function(widget){

    self.functions.widgetRun(widget);

    return true;

});

window.daData.init.push(function(widget){

    return true;

});

window.daData.bind_actions.push(function(widget){

    return true;

});

window.daData.settings.push(function(widget){
    // Скрытие полей для хранения настроек виджета
    self.functions.hideOptionsFields();

    // Получение настроек с сервера
    var req = self.functions.getWidgetSettings(widget);
    req.then(function(w_settings){
        // Сохранение пользовательских настроек
        widget.params.personalSettings = (w_settings.personalSettings) ? w_settings.personalSettings : {};
        
        // Отображение элементов настроек виджета
        //self.functions.showFrame(widget, w_settings);
    });
    
    // Отображение элементов настроек виджета
    self.functions.showFrame(widget);

    return true;
});

window.daData.onSave.push(function(widget){
    // Сбор всех настроек в один объект
    var w_settings = {
        "subdomain": widget.system().subdomain,
        "settings": {
            "scopes": JSON.parse($('.widget_settings_block__input_field input[name=scopes]').val()),
            "users": JSON.parse($('.widget_settings_block__input_field input[name=users]').val()),
            "contacts": JSON.parse($('.widget_settings_block__input_field input[name=contacts]').val()),
            "companies": JSON.parse($('.widget_settings_block__input_field input[name=companies]').val()),
            "leads": JSON.parse($('.widget_settings_block__input_field input[name=leads]').val()),
            "personalSettings": widget.params.personalSettings
        }
    };
    
    // Отправка настроек на сервер
    var req = $.post("https://itproblem.net/dev/team/handlers/set_settings.php", w_settings, 'json');
    
    req.done(function(){
        console.log('OK');
    });

    return true;
});

window.daData.destroy.push(function(){
    //TODO
    return true;
});

window.daData.contacts.push(function(){
    //TODO
    return true;
});

window.daData.leads.push(function(){
    //TODO
    return true;
});

window.daData.tasks.push(function(){
    //TODO
    return true;
});

// Дополнительные методы виджета
var functions = {
    // Скрытие полей с для хранения настроек
    hideOptionsFields: function(){
        $('.widget_settings_block__item_field input[name="companies"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();
        $('.widget_settings_block__item_field input[name="contacts"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();
        $('.widget_settings_block__item_field input[name="leads"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();
        $('.widget_settings_block__item_field input[name="users"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();
        $('.widget_settings_block__item_field input[name="scopes"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();

        // На данный момент эти поля не используются
        $('.widget_settings_block__item_field input[name="api"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();
        $('.widget_settings_block__item_field input[name="fields"]').closest('.widget_settings_block__item_field').css('opacity', '0').hide();
    },

    // Структура блока настроек
    showFrame: function(widget){
        var html = '<link rel="stylesheet" type="text/css" href="https://itproblem.net/dev/team/handlers/css/tabs.css">' +
                   '<link rel="stylesheet" type="text/css" href="https://itproblem.net/dev/team/handlers/css/chosen.css">' +
                   '<link rel="stylesheet" type="text/css" href="https://itproblem.net/dev/team/handlers/css/scopes_collapse.css">' +
                   '<link rel="stylesheet" type="text/css" href="https://itproblem.net/dev/team/handlers/css/switcher.css">' +
                   '<link rel="stylesheet" type="text/css" href="https://itproblem.net/dev/team/handlers/css/users_collapse.css">' +
                   '<div id="scopes_list"></div>' +
                   '<div id="users_list"></div>' +
                   '<div id="tabs_block"></div>';

        // Отображение после базовых полей виджета
        $('.widget_settings_block__item_field:last').after(html);
        
        // Вывод коллапса со списком сущностей
        self.functions.scopesList(widget);
        
        // Запрос к API amoCRM для получения списков полей и пользователей
        $.get('https://' + AMOCRM.widgets.system.subdomain + '.amocrm.ru/private/api/v2/json/accounts/current', function (data) {
            // Вывод коллапса со списком пользователей системы
            self.functions.usersList(widget, data);
            
            // Вывод вкладок с элементами для сопоставления полей amoCRM и dadata.ru
            self.functions.tabsComparisonLines(widget, data);
        });
    },

    // Вывод списка сущностей с чекбоксами
    scopesList: function(widget){
        // Формирование разметки блока со списком сущностей
        var html =  '<div id="scopes_collapse">' +
                        '<h3>Сущности</h3>' +
                        '<div>';

        // Формирование разметки элементов списка с использованием CSS3-свитчера
        html += '<div class="scopes_list_row">' +
                    '<label class="switch switch-yes-no">' +
                        '<input class="switch-input" type="checkbox" name="SCOPE" value="comcard">' +
                        '<span class="switch-label" data-on="ДЕАКТИВИРОВАТЬ" data-off="АКТИВИРОВАТЬ"></span>' +
                        '<span class="switch-handle"></span>' +
                    '</label>' +
                    '<span class="scope_name">Компании</span>' +
                '</div>';

        html += '<div class="scopes_list_row">' +
                    '<label class="switch switch-yes-no">' +
                        '<input class="switch-input" type="checkbox" name="SCOPE" value="ccard">' +
                        '<span class="switch-label" data-on="ДЕАКТИВИРОВАТЬ" data-off="АКТИВИРОВАТЬ"></span>' +
                        '<span class="switch-handle"></span>' +
                    '</label>' +
                    '<span class="scope_name">Контакты</span>' +
                '</div>';

        html += '<div class="scopes_list_row">' +
                    '<label class="switch switch-yes-no">' +
                        '<input class="switch-input" type="checkbox" name="SCOPE" value="lcard">' +
                        '<span class="switch-label" data-on="ДЕАКТИВИРОВАТЬ" data-off="АКТИВИРОВАТЬ"></span>' +
                        '<span class="switch-handle"></span>' +
                    '</label>' +
                    '<span class="scope_name">Сделки</span>' +
                '</div>';

        html += '</div></div>';

        // Отображение внутри соответствующего контейнера
        $('#scopes_list').append(html);

        // Активация коллапс-блока
        $('#scopes_collapse').collapse({});

        // Активация отслеживания изменений статусов чекбоксов и запись изменений в соответствующее поле настроек
        $('#scopes_list input[type=checkbox]').on('click', function () {
            var result = {};
            $('#scopes_list input[type=checkbox]').each(function() {
                var cb_value = $(this).val();
                var cb_status = $(this).prop('checked');
                cb_status = (cb_status) ? 'on' : 'off';
                result[cb_value] = cb_status;
            });
            $('.widget_settings_block__input_field input[name=scopes]').val(JSON.stringify(result));
        });

        // Установка чекбоксов в настройках согласно конфигурации
        var scopes = (widget.params.scopes) ? JSON.parse(widget.params.scopes) : {};
        for (var scope in scopes){
            if (scopes[scope] === 'on') {
                $('#scopes_list input[value="' + scope + '"]').attr('checked', '');
            }
        }
    },

    // Вывод списка пользователей системы с чекбоксами
    usersList: function(widget, data){
        // Список пользователей из ответа API amoCRM
        var users = (data['response']['account']['users']) ? data['response']['account']['users'] : {};

        // Формирование разметки блока со списком пользователей
        var html =  '<div id="users_collapse">' +
                        '<h3>Пользователи</h3>' +
                        '<div>';

        // Формирование элементов списка
        for (var user in users){
            html += '<div class="users_list_row">' +
                        '<label class="switch switch-yes-no">' +
                            '<input class="switch-input" type="checkbox" name="USER" value="' + users[user]['id'] + '">' +
                            '<span class="switch-label" data-on="ДЕАКТИВИРОВАТЬ" data-off="АКТИВИРОВАТЬ"></span>' +
                            '<span class="switch-handle"></span>' +
                        '</label>' +
                        '<span class="user_name">' + users[user]['name'] + ' ' + users[user]['last_name'] + '</span>' +
                    '</div>';
        }

        html += '</div></div>';

        // Отображение внутри соответствующего контейнера
        $('#users_list').append(html);

        // Активация коллапс-блока
        $("#users_collapse").collapse({});

        // Активация отслеживания изменений статусов чекбоксов и запись изменений в соответствующее поле настроек
        $('#users_list input[type=checkbox]').on('click', function () {
            var result = {};
            $('#users_list input[type=checkbox]').each(function() {
                var cb_value = $(this).val();
                var cb_status = $(this).prop('checked');
                cb_status = (cb_status) ? 'on' : 'off';
                result[cb_value] = cb_status;
            });
            $('.widget_settings_block__input_field input[name=users]').val(JSON.stringify(result));
        });

        // Установка чекбоксов в настройках согласно конфигурации
        var users = (widget.params.users) ? JSON.parse(widget.params.users) : {};
        for (var user in users){
            if (users[user] === 'on') {
                $('#users_list input[value="' + user + '"]').attr('checked', '');
            }
        }
    },

    // Отображение вкладок со списками полей и чекбоксами
    tabsComparisonLines: function(widget, data){
        // Формирование разметки блока вкладок
        var html = '<div id="tabs">' +
                       '<ul class="tabs">' +
                           '<li class="tab active"><a href="#contacts" class="active">Контакты</a></li>' +
                           '<li class="tab"><a href="#companies">Компании</a></li>' +
                           '<li class="tab"><a href="#leads">Сделки</a></li>' +
                       '</ul>' +
                       '<div class="tabs-content">' +
                           '<div id="contacts">';

        // Кнопка добавления элемента списка сопоставления полей
        html += self.functions.showComparisonAddButton();

        // Формирование элементов списка для контактов на основании настроек виджета
        var w_contacts = (widget.params.contacts) ? JSON.parse(widget.params.contacts) : {};
        for (var id in w_contacts) {
            html += self.functions.showComparisonLine(data, 'contacts', id, w_contacts[id]);
        }

        html += '</div>' +
                '<div id="companies">';
        
        // Кнопка добавления элемента списка соответсивя полей
        html += self.functions.showComparisonAddButton();

        // Формирование элементов списка для компаний на основании настроек виджета
        var w_companies = (widget.params.companies) ? JSON.parse(widget.params.companies) : {};
        for (var id in w_companies) {
            html += self.functions.showComparisonLine(data, 'companies', id, w_companies[id]);
        }

        html += '</div>' +
            '<div id="leads">';
            
        // Кнопка добавления элемента списка соответсивя полей
        html += self.functions.showComparisonAddButton();

        // Формирование элементов списка для сделок на основании настроек виджета
        var w_leads = (widget.params.leads) ? JSON.parse(widget.params.leads) : {};
        for (var id in w_leads) {
            html += self.functions.showComparisonLine(data, 'leads', id, w_leads[id]);
        }
            
        html += '</div></div></div>';

        // Отображение внутри соответствующего контейнера
        $('#tabs_block').append(html);
            
        // Активация выпадающих списков
        self.functions.selectActivate();

        // Активация интерактивности вкладок
        self.functions.tabsActivate();
        
        // Слежение за изменениями в сопоставлении полей
        self.functions.checkSelect();
        
        // Добавление элемента сопоставления полей во вкладку контактов
        $('#contacts .add_comparison').on('click', function(e){
            e.preventDefault();
            $(this).parent('.comparisons_list_row').after(self.functions.showComparisonLine(data, 'contacts'));
            
            // Активация выпадающих списков
            self.functions.selectActivate();
            
            // Активация кнопок удаления элементов сопоставления полей
            self.functions.comparisonDelete();
            
            // Активация кнопок добавления полей в amoCRM
            self.functions.addFieldToAmo(data);
            
            // Слежение за изменениями в сопоставлении полей
            self.functions.checkSelect();
        });
        
        // Добавление элемента сопоставления полей во вкладку компаний
        $('#companies .add_comparison').on('click', function(e){
            e.preventDefault();
            $(this).parent('.comparisons_list_row').after(self.functions.showComparisonLine(data, 'companies'));
            
            // Активация выпадающих списков
            self.functions.selectActivate();
            
            // Активация кнопок удаления элементов сопоставления полей
            self.functions.comparisonDelete();
            
            // Активация кнопок добавления полей в amoCRM
            self.functions.addFieldToAmo(data);
            
            // Слежение за изменениями в сопоставлении полей
            self.functions.checkSelect();
        });
        
        // Добавление элемента сопоставления полей во вкладку сделок
        $('#leads .add_comparison').on('click', function(e){
            e.preventDefault();
            $(this).parent('.comparisons_list_row').after(self.functions.showComparisonLine(data, 'leads'));
            
            // Активация выпадающих списков
            self.functions.selectActivate();
            
            // Активация кнопок удаления элементов сопоставления полей
            self.functions.comparisonDelete();
            
            // Активация кнопок добавления полей в amoCRM
            self.functions.addFieldToAmo(data);
            
            // Слежение за изменениями в сопоставлении полей
            self.functions.checkSelect();
        });
        
        // Активация кнопок удаления элементов сопоставления полей
        self.functions.comparisonDelete();
        
        // Активация кнопок добавления полей в amoCRM
        self.functions.addFieldToAmo(data);
    },
    
    // Вывод на экран блока сопоставления полей amoCRM и dadata.ru
    showComparisonLine: function(amocrm_fields, entity, amo_id = '', dadata_str = ''){
        // Список дополнительных полей amoCRM из определённой сущности
        var fields = (amocrm_fields['response']['account']['custom_fields'][entity]) ? amocrm_fields['response']['account']['custom_fields'][entity] : {};
        
        // Соответствие русскоязычных названий сущностей
        var entity_names = {
            contacts:  "Контакты",
            companies: "Компании",
            leads:     "Сделки"
        };
        
        // Элемент списка соответсивя полей
        var html = '<div class="comparisons_list_row">';
        
        // Выпадающий список с полями amoCRM
        html += '<select class="chosen-select amocrm_select" name="amocrm_field" data-placeholder="amoCRM">';
        html += '<option value=""></option>';
        html += '<optgroup label="' + entity_names[entity] + '">';
        
        // Добавление элементов в список с подстановкой атрибута select при соответствии с настройками
        var selected = "";
        for (var i = 0; i < fields.length; i++){
            if (amo_id == fields[i].id) {
                selected = "selected";
            }
            html += '<option value="' + fields[i].id + '" ' + selected + '>' + fields[i].name + '</option>';
            selected = "";
        }
        
        html += '</optgroup>';
        html += '</select>';
        
        // Выпадающий список с полями dadata.ru
        html += '<select class="chosen-select dadata_select" name="dadata_field" data-placeholder="Подсказчик dadata">';
        html += '<option value=""></option>';
        html += '<optgroup label="ФИО">';
        
        // Добавление элементов в список с подстановкой атрибута select при соответствии с настройками
        var selected = "";
        for (var field in self.daDataFields){
            if (dadata_str == field) {
                selected = "selected";
            }
            if (field.indexOf('fio') == 0){
                html += '<option value="' + field + '" ' + selected + '>' + self.daDataFields[field] + '</option>';
            }
            selected = "";
        }
        
        html += '</optgroup>';
        html += '<optgroup label="Адрес">';
        
        // Добавление элементов в список с подстановкой атрибута select при соответствии с настройками
        var selected = "";
        for (var field in self.daDataFields){
            if (dadata_str == field) {
                selected = "selected";
            }
            if (field.indexOf('address') == 0){
                html += '<option value="' + field + '" ' + selected + '>' + self.daDataFields[field] + '</option>';
            }
            selected = "";
        }
        
        html += '</optgroup>';
        html += '<optgroup label="Компания">';
        
        // Добавление элементов в список с подстановкой атрибута select при соответствии с настройками
        var selected = "";
        for (var field in self.daDataFields){
            if (dadata_str == field) {
                selected = "selected";
            }
            if (field.indexOf('party') == 0){
                html += '<option value="' + field + '" ' + selected + '>' + self.daDataFields[field] + '</option>';
            }
            selected = "";
        }
        
        html += '</optgroup>';
        html += '<optgroup label="Банк">';
        
        // Добавление элементов в список с подстановкой атрибута select при соответствии с настройками
        var selected = "";
        for (var field in self.daDataFields){
            if (dadata_str == field) {
                selected = "selected";
            }
            if (field.indexOf('bank') == 0){
                html += '<option value="' + field + '" ' + selected + '>' + self.daDataFields[field] + '</option>';
            }
            selected = "";
        }
        
        html += '</optgroup>';
        html += '</select>';
        
        // Кнопка добавление поля в amoCRM
        html += '<a class="add_field_to_amo" name="' + entity + '" href="#" title="Добавить поле в amoCRM"><span class="icon icon-plus"></span></a>';
        
        // Кнопка удаления сопоставления полей
        html += '<a class="delete_comparison" href="#" title="Удалить сопоставление полей"><span class="icon icon-trash"></span></a>';
        
        html += '</div>';
        
        return html;
    },
    
    // Кнопка добавления элемента списка соответсивя полей
    showComparisonAddButton: function() {
        var html = '<div class="comparisons_list_row">' +
                        '<a href="#" class="add_comparison">Добавить сопоставление</a>' +
                    '</div>';
        
        return html;
    },
    
    // Активация интерактивности вкладок
    tabsActivate: function() {
        var tabs = $('#tabs');
        $('.tabs-content > div', tabs).each(function(i){
            if ( i != 0 ) $(this).hide(0);
        });
        tabs.on('click', '.tabs a', function(e){
            e.preventDefault();
            var tabId = $(this).attr('href');
            $('.tabs a',tabs).removeClass();
            $(this).addClass('active');
            $('.tabs-content > div', tabs).hide(0);
            $(tabId).show();
        });
    },
    
    // Активация выпадающих списков
    selectActivate: function() {
        $('.chosen-select').chosen({
            no_results_text: "Совпадений нет",
            allow_single_deselect: true,
            search_contains: true
        });
    },
    
    // Удаление элемента сопоставления полей по нажатию на кнопку
    comparisonDelete: function() {
        $('.delete_comparison').unbind('click');
        $('.delete_comparison').on('click', function(e){
            e.preventDefault();
            $(this).parent('.comparisons_list_row').remove();
            $('.chosen-select').trigger('change');
        
            // Если удалены все строки сопоставления полей
            var comp_lines = $('#contacts .comparisons_list_row');
            if (comp_lines.length === 1) {
                $('.widget_settings_block__input_field input[name=contacts]').val(JSON.stringify({}));
            }
            
            comp_lines = $('#companies .comparisons_list_row');
            if (comp_lines.length === 1) {
                $('.widget_settings_block__input_field input[name=companies]').val(JSON.stringify({}));
            }
            
            comp_lines = $('#leads .comparisons_list_row');
            if (comp_lines.length === 1) {
                $('.widget_settings_block__input_field input[name=leads]').val(JSON.stringify({}));
            }
        });
    },
    
    // Добавление поля в amoCRM по нажатию на кнопку
    addFieldToAmo: function(amo_response) {
        // Соответствие названий сущностей их кодам в API amoCRM
        var entities = {
            contacts: 1,
            companies: 3,
            leads: 2
        };
        
        // Привязка добавления поля в amoCRM по клику
        $('.add_field_to_amo').unbind('click');
        $('.add_field_to_amo').on('click', function(e){
            e.preventDefault();
            
            // Находящийся рядом со списком полей dadata список полей amoCRM
            var amo_select = $(this).siblings('.amocrm_select');
            
            // Название сущности, в которой произошло нажатие
            var entity = $(this).attr('name');
            
            // Русскоязычное название выбранного из списка dadata поле
            var field_name = $(this).siblings('.dadata_select').val();
            field_name = daDataFields[field_name];
            
            // Набор дополнительных полей определённой сущности из ответа API amoCRM
            var amo_data = (amo_response['response']['account']['custom_fields'][entity]) ? amo_response['response']['account']['custom_fields'][entity] : {};
            
            // Проверка, что выбранный элемент списка dadata не пустой и в сущности amoCRM нет поля с таким же названием
            if (!self.functions.stringIsEmpty(field_name) && !self.functions.existsInObject(amo_data, 'name', field_name)) {
                // Структура запроса на добавление текстового поля в определённую сущность amoCRM
                var request = {
                    "request": {
                        "fields": {
                            "add": [{
                                "name": field_name,
                                "type": 1,
                                "element_type": entities[entity],
                                "disabled": 0,
                                "origin": "ck_dadata_widget_widget_dadata_ck"
                            }]
                        }
                    }
                };
                
                // Запрос на добавление поля в amoCRM
                $.ajax({
                    url: 'https://' + AMOCRM.widgets.system.subdomain + '.amocrm.ru/private/api/v2/json/fields/set',
                    type: 'post',
                    data: JSON.stringify(request),
                    success: function(data) {
                        // ID созданного поля
                        var field_id = (data['response']['fields']['add'][0]['id']) ? data['response']['fields']['add'][0]['id'] : -1;
                        
                        // Если ID было получено...
                        if (field_id !== -1) {
                            // Формирование разметки пункта списка с данными нового поля
                            var option = '<option selected value="' + field_id + '">' + field_name + '</option>';
                            
                            // Добавление нового элемента в конец списка полей amoCRM
                            amo_select.append(option);
                            
                            // Перерендеринг списков Chosen
                            $('.chosen-select').trigger('chosen:updated');
                            
                            // Инициация события изменения состояния списков сопоставления полей в определённой вкладке
                            $('#' + entity + ' .chosen-select').trigger('change');
                        }
                    },
                    error: function (data){
                        console.log('ADD FIELD REQUEST ERROR');
                    }
                });
                
            // Если выбранной из списка dadata поле присутствует в ответе API amoCRM...
            } else if (!self.functions.stringIsEmpty(field_name) && self.functions.existsInObject(amo_data, 'name', field_name)) {
                
                // Добавление атрибута selected элементу списка amoCRM с соответствующим русскоязычным названием
                amo_select.find('option:contains(' + field_name + ')').attr('selected', 'selected');
                
                // Перерендеринг списков Chosen
                $('.chosen-select').trigger('chosen:updated');
                
                // Инициация события изменения состояния списков сопоставления полей в определённой вкладке
                $('#' + entity + ' .chosen-select').trigger('change');
            }
        });
    },
    
    // Слежение за изменениями в сопоставлении полей и запись при изменении состояния
    checkSelect: function() {
        $('.chosen-select').unbind('change');
        $('#contacts .chosen-select').bind('change', function () {
            var result = {};
            
            // Перебор каждого элемепнта списка сопоставления вкладки контактов кроме первого (в первом находится кнопка)
            $('#contacts .comparisons_list_row').not(':first').each(function(){
                // Значение выбранного поля из списка полей amoCRM
                var amo = $(this).children('.amocrm_select').val();
                
                // Значение выбранного поля из списка полей dadata.ru
                var dadata = $(this).children('.dadata_select').val();
                
                // Если поля не пустые они добавляются в результирующий объект
                if (!self.functions.stringIsEmpty(amo) && !self.functions.stringIsEmpty(dadata)) {
                    result[amo] = dadata;
                }
            });
            
            // Запись результата в соответствующее поле настроек
            $('.widget_settings_block__input_field input[name=contacts]').val(JSON.stringify(result));
        });
        
        $('#companies .chosen-select').bind('change', function () {
            var result = {};
            
            // Перебор каждого элемепнта списка сопоставления вкладки компаний кроме первого (в первом находится кнопка)
            $('#companies .comparisons_list_row').not(':first').each(function(){
                // Значение выбранного поля из списка полей amoCRM
                var amo = $(this).children('.amocrm_select').val();
                
                // Значение выбранного поля из списка полей dadata.ru
                var dadata = $(this).children('.dadata_select').val();
                
                // Если поля не пустые они добавляются в результирующий объект
                if (!self.functions.stringIsEmpty(amo) && !self.functions.stringIsEmpty(dadata)) {
                    result[amo] = dadata;
                }
            });
            
            // Запись результата в соответствующее поле настроек
            $('.widget_settings_block__input_field input[name=companies]').val(JSON.stringify(result));
        });
        
        
        $('#leads .chosen-select').bind('change', function () {
            var result = {};
            
            // Перебор каждого элемепнта списка сопоставления вкладки сделок кроме первого (в первом находится кнопка)
            $('#leads .comparisons_list_row').not(':first').each(function(){
                // Значение выбранного поля из списка полей amoCRM
                var amo = $(this).children('.amocrm_select').val();
                
                // Значение выбранного поля из списка полей dadata.ru
                var dadata = $(this).children('.dadata_select').val();
                
                // Если поля не пустые они добавляются в результирующий объект
                if (!self.functions.stringIsEmpty(amo) && !self.functions.stringIsEmpty(dadata)) {
                    result[amo] = dadata;
                }
            });
            
            // Запись результата в соответствующее поле настроек
            $('.widget_settings_block__input_field input[name=leads]').val(JSON.stringify(result));
        });
    },
    
    // Проверка строки на пустоту
    stringIsEmpty: function(str) {
        if (str.trim() === '') {
            return true;
        }
        
        return false;
    },
    
    // Проверка наличия параметра с определённым значением в объекте
    existsInObject: function(obj, param, val) {        
        for (var index in obj) {
            if (obj[index][param] === val) {
                return true;
            }
        }
        
        return false;
    },
    
    //Запускает логику виджета 
    widgetRun: function(widget){

        //Если мы в настройках заканчиваем работу
        if (widget.system().area==='settings') return false;
        
        //Получение данных с сервера. getWidgetSettings возвращает jquery deferred object
        self.functions.getWidgetSettings(widget)
        .then(function(settings){

            

            console.log(settings);

            /*

            settings - данные полученые с сервера. Структура такая же как у widget.params, 
            но есть дополнительное поле widget.params.personalSettings

            personalSettings - поле в котором хранится информация для конкретных пользователей для каких сущностей виджет включен

            personalSettings = {
                    "#USERID":{
                        "#AREA": "on"/"off",
                        "#AREA": "off"/"off",
                        ...
                    },
                    ...
            }
            
            */


            //Добавляем поле personalSettings к параметрам виджета, либо пустой обьект если виджет запускается впервые
            widget.params.settings = settings;
            widget.params.settings.personalSettings = settings.personalSettings || {};

            
            var w_code = widget.get_settings().widget_code,
                area = widget.system().area;
            
            
            
            //Если в настройках виджета для текущего пользователя или сущности виджет неактивен, то прекращаем работу и выходим из функции
            if (!self.functions.isEnableForEntity(widget) || !self.functions.isEnableForUser(widget)) return false;
            
            //Проверяем состояние виджета если он активен подставляем checked для переключения чекбокса в нужное состояние
            var checked = self.functions.switcherIsOn(widget)? 'checked':'';
            console.log("checked ", checked);
            
            //Рендерим разметку
            var html_data ='<style>.right_side_'+w_code+'_wrap{background-color:#fff;}\
            .card-widgets__widget__body.js-body-hide{background-color:#f9f9f9;}\
            #'+w_code+'_help{ margin:10px 35px 10px 0; cursor:pointer; color:#f9f9f9; font-style:italic; font-weight:bold; font-size:22px; text-align:center; width:30px; height:30px; display:block; float:right; background:#d9d9d9; border-radius:5px; outline:none; }\
            #'+w_code+'_help:hover,#'+w_code+'_help:focus{ color:#fff; background-color:#c2c2c2; transition:background-color.02s; }#'+w_code+'_help_tool_tip{z-index: 9999;display: none;top:0px;left:0px;background-color: #000;padding: 5px 10px 5px 10px;color: white;opacity: 0.6;border-radius: 5px; margin: 5px;} </style>\
            <link type="text/css" rel="stylesheet" href="https://itproblem.net/dev/team/handlers/css/switcher.css">\
            <label style="margin:10px 11px 10px 20px;display:inline-block;"  class="switch switch-yes-no"><input id="'+w_code+'_switcher_checkbox" class="switch-input" type="checkbox" '+checked+'><span class="switch-label" data-on="ДЕАКТИВИРОВАТЬ" data-off="АКТИВИРОВАТЬ"></span><span class="switch-handle"></span></label><button id="'+w_code+'_help">i</button><div id="'+w_code+'_help_tool_tip"></div>';
            
            widget.render_template({
                    caption:{
                    class_name:'right_side_'+w_code+'_wrap',
                },
                body: html_data,
                render : '' 
            });  
        })
        //Если не удалось запустить виджет показываем ошибку
        .fail(function(err){
            alert("Произошла ошибка, не удалось запустить виджет. Приносим свои извинения за неудобство");
        })
        .then(function(){
            //Биндим кнопки подсказки и активации\деактивации
            self.functions.bindRightSideButton(widget);
        })
        .then(function(){
            //если виджет активен для сущности и пользователя то запускаем функцию активирующую автодополнение
            if (!self.functions.isEnableForEntity(widget) || !self.functions.isEnableForUser(widget)) return;
            self.functions.autofill(widget);
        })
        
    },
    //Навешивание событий на кнопку помощи и чекбокс
    bindRightSideButton: function(widget){

        var w_code = widget.get_settings().widget_code

        var switchButton = '#'+w_code+'_switcher_checkbox';
        var helpButton = '#'+w_code+'_help';
        var tooltipDiv= '#'+w_code+'_help_tool_tip';
        
        $(switchButton).click(function() {

            /*
            Проверяем состояние чекбокса
             
            !$(this).prop("checked") потому что текущее состояние это состояние после клика

            */
            var switcherWasOn = !$(this).prop("checked");
            
            // Запускаем функции отключающие либо включающие виджет для текущей сущности
            if (switcherWasOn) {
                self.functions.disableWidget(widget);
        
            }else{
                self.functions.enableWidget(widget);
            }
        });

         
         
        $(helpButton).mousemove(function (e) {
            
            var area = widget.system().area;
            //Проверяем состояние чекбокса, взависимости от него подставляем нужное слово во фразу
            var isOn = $('#'+w_code+'_switcher_checkbox').prop( "checked" )? 'включен.':'выключен.';
            
            switch(area) {
            //формируем фразу
            case 'ccard':
                var phrase = "Для контактов виджет - " + isOn;
                break;
            case 'lcard':
                var phrase = "Для сделок виджет - " + isOn;
                break;
            case 'comcard':
                var phrase = "Для компаний виджет - " + isOn;
                break;
            };
            
            var data = phrase;
            $(tooltipDiv).text(phrase).show();
        })
        .mouseout(function () {
            $(tooltipDiv).hide();                 
        });
        
    },

    disableWidget : function(widget){

        var userId = widget.system().amouser_id;
        var entyty = widget.system().area;
        var persSettings = widget.params.settings.personalSettings || {};
        
        
        //URL на который уходит запрос
        var url = "https://itproblem.net/dev/team/handlers/set_settings.php";
        
        //Что отправляем в теле запроса
        var data = {
            'subdomain' : widget.system().subdomain,
            'settings' : widget.params.settings
        };

        //Если в поле persSettings берем существующий userId, либо добавляем если до этого пользователь неразу не отключал(включал) логику 
        persSettings[userId] = persSettings[userId] || {};
        //Для текущей сущности устанавливаем false при проверки она будет считаться неактивной 
        persSettings[userId][entyty] = 'off';

        //Добавляем обновленное поле в данные которые отправятся на сервер
        data.settings.personalSettings = persSettings;
        //Обновляем params в виджете
        widget.params.settings.personalSettings = persSettings;
        
        console.log(data);

        var req = $.post(url,data,'json');
        req.done(function(data){
            console.log("Выключение");
        })
    },


    enableWidget : function(widget){
        var userId = widget.system().amouser_id;
        var entyty = widget.system().area;
        var persSettings = widget.params.settings.personalSettings;
        
        //URL на который уходит запрос
        var url = "https://itproblem.net/dev/team/handlers/set_settings.php"; 

        //Что отправляем в теле запроса
        var data = {
            'subdomain' : widget.system().subdomain,
            'settings' : widget.params.settings
        };
        

        //Если в поле persSettings берем существующий userId, либо добавляем если до этого пользователь неразу не отключал(включал) логику 
        persSettings[userId] = persSettings[userId] || {};

        //Для текущей сущности устанавливаем true при проверки она будет считаться активной 
        persSettings[userId][entyty] = "on";
        
        //Добавляем обновленное поле в данные которые отправятся на сервер
        data.settings.personalSettings = persSettings;

        //Обновляем params в виджете
        widget.params.settings.personalSettings = persSettings;
        
        console.log(data);
        var req = $.post(url,data,'json');
        req.done(function(data){
            console.log("Включение");
        })
    },

    //Возвращает true если для текущей сущности виджет включен, иначе false.
    isEnableForEntity : function(widget){
       
        var area = widget.system().area;
        var scopes = widget.params.settings.scopes; 
        var isActive = false;
        
        $.each(scopes,function(entyty, val){
            if (entyty===area && val === 'on') isActive = true;
        })
        
        return isActive;
    },
    //Возвращает true если для текущего пользователя виджет включен, иначе false.
    isEnableForUser : function(widget){

        var currentUser = widget.system().amouser_id;
        var listUsers = widget.params.settings.users;
        var isActive = false;


        $.each(listUsers,function(user, val){
            if (user===currentUser && val === 'on') isActive = true;
        })
        
        return isActive;
    },
    
    //Проверка включена ли логика для сущности у текущего пользователя(в personalSettings)
    switcherIsOn: function(widget){
        var area = widget.system().area;
        var userId = widget.system().amouser_id;
      
        //если в personalSettings нет ключа userId то считаем что для текущей сущности логика включена
        if (widget.params.settings.personalSettings[userId]===undefined) {
            var persSet = true; 
        //если в personalSettings[userId]нет ключа area то считаем что для текущей сущности логика включена
        }else if(widget.params.settings.personalSettings[userId][area]===undefined) {
            var persSet = true;
        }else {
            //если же есть значение то берем его
            var persSet = widget.params.settings.personalSettings[userId][area];
        }

        if (persSet===true || persSet==='on') return true;
        
        return false;
    },

    //Запрос на сервер получение настроек для текущего субдомена.
    getWidgetSettings: function(widget){
        
        var url = "https://itproblem.net/dev/team/handlers/get_settings.php";

        //возвращаем promise обьект запроса
        return $.post(url,{'subdomain' : AMOCRM.widgets.system.subdomain})
            .then(function(data){
                return JSON.parse(data);
            });
    },
    //Запрос на получение данных от dadata
    dadataPost: function(type,query,token){
        return $.ajax({

            url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/' + type,

            type: 'post',

            data: JSON.stringify({"query": query}),

            headers: {

                "Content-Type": 'application/json',

                "Accept": "application/json",

                "Authorization": "Token " + "39ace945436a81de93173b27f68bf3c4f852ce70"

            },

            dataType: 'json',

            success: function (data) {

                return data;

            },

            error: function (msg){

                alert("Ошибка при обращении к серверу daData.");
            }
        });
    },

    //автодополнение сопоставленных в настройках полей
    autofill: function(widget){
        
        //Получение html списка с подсказками
        function getAutofillList(data,path){
            
            var template = '<div class="'+w_code+'_autofill"><style>.'+w_code+'_autofill_list_wrap{background-color: #eee;overflow: auto;position:absolute;width:100%;z-index:2;}\
            .'+w_code+'_autofill_list{list-style: none; text-decoration: none; padding: 5px 7px;max-height: 200px;}\
            .'+w_code+'_autofill_list li{margin: 5px 0;font-weight:bold;}.'+w_code+'_autofill_list li:hover{background-color:#c2c2c2;cursor:pointer;}\
            .'+w_code+'_autofill_list li .item_value{font-size:12px;font-style:italic;}</style>\
            <div class="'+w_code+'_autofill_list_wrap">\
              <ul class="'+w_code+'_autofill_list"> \
                {% for item in dadata.suggestions %}\
                    <li data-ind="{{ loop.index }}"><p>{{item'+path+'}}</p><p class="item_value">{{item.value}}</p></li>\
                {% endfor %}\
              </ul>\
            </div></div>';
        
            var html = widget.render({data : template},
                  {dadata: data});
            return html;
        };


        //Получает из данных определенных для автоподстановки, поля которые так же относятся к данной сущности
        function getAutofilledField(data,type){
            
            var res = {};
            $.each(data,function(k,v){
                var paths = v.split(':');
                if (paths[0]===type){
                    res[paths.slice(1).join(':')] = k;                        
                }
            })
            return res;
        };

        
        var area = widget.system().area,
            w_code = widget.get_settings().widget_code;
        
        //Получаем данные для автодополнения из нужного поля в настройках
        if (area === 'comcard'){
            var amo2dadata = widget.params.settings.companies;
        };
        if(area === 'lcard'){
            var amo2dadata = widget.params.settings.leads;
        };
        if(area === 'ccard'){
            var amo2dadata = widget.params.settings.contacts;
        };
        
        
        //Для каждого поля для которого вклбючены подсказки вещаем обработчик на нажатие клавиш
        $.each(amo2dadata,function(id,path) {
            //элемент на который вещаем обработчик
            var field = $('[name="CFV['+id+']"]');
            //селектор diva со списком автодополнений
            var autofillArea = '.'+w_code+'_autofill';

            field.data('path', path);

            field.keyup(function(e){

                //если нажаты стрелки или ctrl или alt то ничего не делаем и выходим из функции
                switch(e.witch) {
                    case 16: return ;
                    case 17: return ;
                    case 18: return ;
                    case 37: return ;
                    case 38: return ;
                    case 39: return ;
                    case 40: return ;
                }
                $(autofillArea).remove();

                // если введено меньше 2х символов либо пользователь отключил логику для сущности то выходим
                if (field.val().length < 2||!self.functions.switcherIsOn(widget)) return;

                //получаем массив индексов для доступа к нужному значению в ответе от dadata
                var keys = path.split(':');

                //формируем строку подставляемую в шаблон для доступа к нужному полю в ответе
                var dadataPath = '.'+keys.slice(1).join('.');

                console.log(dadataPath);

                //что запрашиваем от dadata
                var type = keys[0]
                //значение по которому подсказываем
                var query = field.val();
                 
                self.functions.dadataPost(type,query)
                .then(function(data){
                    //получаем ответ от dadata , если пустой то выходим
                    if(data.suggestions.length === 0) return;

                    console.log(data);
                    
                    //получаем поля доступные для автодополнения у выбранного элемента в dadata 
                    var dadata2amo = getAutofilledField(amo2dadata,type);
                    
                    //вставка списка автодополнения
                    field.parent().append(getAutofillList(data,dadataPath));
                    
                    //селектор выбираемого значения
                    var selectVal = '.'+w_code+'_autofill_list li';
                    $(selectVal).click(function(e){
                        
                        console.log(dadata2amo);
                        //индекс выбранного элемента в ответе от dadata
                        var ind = $(this).data('ind')-1;
                        
                        console.log(data.suggestions[ind]);

                        //для каждого поля доступного для автодополнения подставляем значения
                        $.each(dadata2amo,function(k,fieldId){
                            
                            var value = data.suggestions[ind];
                            var keys = k.split(':');
                            
                            keys.forEach(function(key){
                                if (value === null) return;
                                value = value[key];
                            });

                            if(value !== null && value !== "UNKNOWN"){
                                if (value==="FEMALE") {
                                    value = "Ж"
                                }
                                if (value==="MALE") {
                                    value = "М"
                                }

                                $('[name="CFV['+fieldId+']"]').val(value);
                            }

                            //удаляем список 
                            $(autofillArea).remove();
                        })

                    })
                })
            });

            //обработчик убирающий список подсказок если пользователь кликнул вне его элемента
            $(document).click(function(e){

                var found = false;
                var el = $(e.target);
                while(!found){
                    found = el.hasClass('.jasttest_autofill');
                    if (found) return;
                    el = el.parent();
                    if (el.parent().length == 0) break;
                }
                $(autofillArea).remove();
            });
        });
    }

};

// Поля dadata.ru
var daDataFields = {
    "fio:data:gender":                         "Пол",
    "fio:data:name":                           "Имя",
    "fio:data:patronymic":                     "Отчество",
    "fio:data:surname":                        "Фамилия",
    
    "address:data:city":                       "Город",
    "address:data:kladr_id":                   "КЛАДР",
    "address:data:okato":                      "ОКАТО",
    "address:data:oktmo":                      "ОКТМО",
    "address:data:postal_code":                "Индекс",
    "address:data:region_with_type":           "Регион",
    "address:data:street_with_type":           "Улица",
    "address:data:house":                      "Дом",
    "address:data:house_type":                 "Тип здания",
    "address:data:city_district_with_type":    "Район города",
    "address:data:flat":                       "Номер квартиры",
    
    "party:data:inn":                          "ИНН",
    "party:data:kpp":                          "КПП",
    "party:data:ogrn":                         "ОГРН",
    "party:data:okved":                        "ОКВЭД",
    "party:data:okved_type":                   "Тип ОКВЭД",
    "party:data:address:value":                "Адрес (организации)",
    "party:data:management:name":              "ФИО руководителя",
    "party:data:management:post":              "Должность руководителя",
    "party:data:name:short_with_opf":          "Название (организации)",
    
    "bank:data:bik":                           "БИК",
    "bank:data:correspondent_account":         "Кор. счёт",
    "bank:data:swift":                         "СВИФТ",
    "bank:data:name:payment":                  "Название (банка)",
    "bank:data:address:value":                 "Адрес (банка)"
};